noun-compound-analysis
===

Source code used in the research and analysis of noun compound interpretability as part of a senior thesis submitted for the undergraduate degree of Bachelor's of Science in Engineering at Princeton University.

Contact [Charles Marsh](crmarsh@princeton.edu) for more details.

## License

MIT.
