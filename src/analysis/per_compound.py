from nltk.corpus import wordnet as wn
import config
from util import math
import tfidf

model = tfidf.Model()


def is_valid_synset(synset_name):
    return synset_name and '.n.' in synset_name


def is_valid_judgment(judgment):
    return judgment['rating'] != 'Major'


def get_synset_pairs(compound_type):
    synsets = config.SYNSET(compound_type, run=1)

    # Fetch all pairs of synsets
    pairs = []
    for compound in synsets:
        # Remove synsets for empty paraphrases
        assigned_synsets = filter(is_valid_synset, synsets[compound])
        n = len(assigned_synsets)
        for i in range(n):
            s1 = wn.synset(assigned_synsets[i])
            for j in range(i + 1, n):
                s2 = wn.synset(assigned_synsets[j])
                pairs.append((s1, s2))

    return pairs


def get_paraphrase_pairs(compound_type):
    data = config.CLEAN_DATA(run=1)
    compounds = config.SYNSET(compound_type, run=1).keys()

    # Fetch all pairs of synsets
    pairs = []
    for compound in compounds:
        # Remove judgments for empty paraphrases
        judgments = filter(is_valid_judgment, data[compound]['judgments'])
        sds = map(lambda j: j['sd'], judgments)
        n = len(sds)
        for i in range(n):
            for j in range(i + 1, n):
                pairs.append((sds[i], sds[j]))

    return pairs


def synset_distance(s1, s2):
    return s1.shortest_path_distance(s2)


def paraphrase_distance(sd1, sd2):
    return model.similarity(sd1, sd2)


if __name__ == '__main__':
    # Extract pairs
    none_synset_pairs = get_synset_pairs(config.NONE)
    minor_synset_pairs = get_synset_pairs(config.MINOR)

    # Map to distances
    none_synset_distances = map(lambda s: synset_distance(*s), none_synset_pairs)
    minor_synset_distances = map(lambda s: synset_distance(*s), minor_synset_pairs)

    # Analyze averages
    print "Average distance between `None` judgments:", math.average(none_synset_distances)
    print "Percentage of same-synset `None` judgments:", math.baseline(none_synset_distances)
    print "Average distance between `Minor` judgments:", math.average(minor_synset_distances)
    print "Percentage of same-synset `Minor` judgments:", math.baseline(minor_synset_distances)

    # Extract paraphrases
    none_paraphrase_pairs = get_paraphrase_pairs(config.NONE)
    minor_paraphrase_pairs = get_paraphrase_pairs(config.MINOR)

    # Map to distances
    none_paraphrase_distances = map(lambda s: paraphrase_distance(*s), none_paraphrase_pairs)
    minor_paraphrase_distances = map(lambda s: paraphrase_distance(*s), minor_paraphrase_pairs)

    print "Average similarity score between `None` paraphrases:", math.average(none_paraphrase_distances)
    print "Average similarity score between `Minor` paraphrases:", math.average(minor_paraphrase_distances)
