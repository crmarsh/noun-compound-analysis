import re
import inflect
import config

prepositions = set([
    'from',
    'of',
    'in',
    'at',
    'on',
    'for',
    'about',
    'to',
    'with'
])

p = inflect.engine()

pre_process = {
    'hand food': {
        'can be eaten with': 'that can be eaten with'
    },
    'cathedral performance': {
        'A cathedral performance is a performance given by cathedras': 'A cathedral performance is a performance that is given by a cathedral'
    },
    'hotel model': {
        'A spokesmodel for a hotel': 'that is a spokesmodel for a hotel',
    },
    'community stock': {
        'Stock owned by the/a community': 'that is owned by the/a community'
    },
    'government structure': {
        'structure that government keep': 'structure that the government keeps'
    },
    'horse war': {
        'War using horses': 'war that uses horses'
    },
    'faculty remedy': {
        'something to fix a problem that exists within the faculty': 'that fixes a problem that exists within the faculty',
        'is recommended': 'is recommended by the'
    },
    'child fish': {
        'fish that are young ?': 'that are young like'
    },
    'drug order': {
        'are specific for': 'that are specific for'
    },
    'testtube height': {
        'the height of the test tube': 'the height of the testtube',
    },
    'exhibition competition': {
        'an exhibition competition is a competition that helds exhibitions': 'that holds'
    },
    'decision reconstruction': {
        'a decision reconstruction is a reconstruction that rocontructs a decision': 'a decision reconstruction is a reconstruction that reconstructs a decision'
    },
    'sports assessment': {
        'a sports assessment is an assessment that is about ports': 'a sports assessment is an assessment that is about sports'
    },
    'operating roaster': {
        'the roaster [is] operating': 'is'
    },
    'engineering partnership': {
        'A partnership for engineering': 'for'
    },
    'bed colonies': {
        'ARE FILLED WITH BEDS': 'that are filled with'
    },
    'extension politician': {
        'political reasons for doing something that are not entirely clear': 'with political reasons for doing something that are not entirely clear'
    },
    'press time': {
        'people talk to the': 'that people talk to the'
    },
    'system signals': {
        'work with the': 'that work with the',
        'indicate the health of a': 'that indicate the health of a',
        'are sent to or from a': 'that are sent to or from a'
    },
    'palladium bomb': {
        'bomb made with palladium': 'that is made with palladium',
        'made of': 'that is made of'
    },
    'faculty tablet': {
        'can be written on by': 'that can be written on by'
    },
    'margin headquarters': {
        'are with in the': 'that are within the'
    },
    'house shelter': {
        'shelter found in a house': 'that is found in a'
    }
}
final = {
    # These deviate from the format in some acceptable but unpredictable way
    'testtube height': {
        'describes, or measures, the height of': 'a testtube height is a height that describes, or measures, the height of a testtube(s)'
    },
    'hotel model': {
        'A spokesmodel for a hotel': 'a hotel model is a model that is a spokesmodel for a hotel(s)'
    },
    'air work': {
        'An air work is a work that involve aircraft maintenance'
    },
    'pet members': {
        'members with special \'pet\' privileges': 'pet members are members with special \'pet\' privileges'
    },
    'country sugar': {
        'is commonly used in the [like raw or brown sugar maybe]': 'a country sugar is a sugar that is commonly used in the country like raw or brown sugar maybe'
    },
    'pickup disease': {
        'a pickup disease is a disease that gets obtained easily by people quickly': 'a pickup disease is a disease that gets obtained easily by people quickly'
    },
    'motor youth': {
        'rides a motorcycle': 'a motor youth is a youth that rides a motorcycle'
    },
    'amateur industry': {
        'An amateur industry is an industry that employs people who are just starting out in their profession': 'an amateur industry is an industry that employs people who are just starting out in their profession'
    },
    'player market': {
        'A player market might refer to a place where gamers can buy/sell merchandise': 'a player market is a place where gamers can buy/sell merchandise'
    },
    'exhibition socket': {
        'An exhibition socket is a prototype socket that is being displayed as a possible future product.': 'an exhibition socket is a prototype socket that is being displayed as a possible future product'
    },
    'start control': {
        'a start control is a control which guides the starting point': 'a start control is a control which guides the starting point'
    },
    'engineering partnership': {
        'partnership that is made up of engineers': 'an engineering partnership is a partnership that is made up of engineers'
    },
    'citizen sombrero': {
        'Mexicans in Mexico wear': 'a citizen sombrero is a sombrero that mexicans in mexico wear'
    },
    'popcorn eye': {
        'A popcorn eye is a [fake] eye that is made of popcorn.': 'a popcorn eye is a fake eye that is made of popcorn(s)'
    },
    'anger members': {
        'anger members are members that manage their anger. (members of an ager management group)': 'anger members are members that manage their anger, like members of an anger management group'
    },
    'major pressure': {
        'Major pressure is pressure that is large (or major) in quantity)': 'a major pressure is a pressure that is large (or major) in quantity'
    },
    'cooperative party': {
        'Cooperative party is a group (or party) that cooperates.': 'a cooperative party is a party that is a group (or party) that cooperates'
    },
    'solvency survey': {
        'a solvency survey is a survey that is taken in each business to know long-term financial obligations': 'a solvency survey is a survey that is taken in each business to know long-term financial obligations'
    },
    'card application': {
        'A card application is a form that required prior to obtaining a line of credit': 'a card application is an application that is a form that required prior to obtaining a line of credit'
    },
    'deficiency member': {
        'A member that is deficient': 'a deficiency member is a member that is deficient'
    },
    'account counselor': {
        'counselor for a financial account, consultant': 'an account counselor is a counselor for a financial account, consultant'
    },
    'bank utensils': {
        'implements used in banking': 'bank utensils are implements used in banking'
    },
    'payroll arrangement': {
        'A payroll arrangement is an arrangement that involves an agreement of how someone or a group of people will be paid for their work': 'a payroll arrangement is an arrangement that involves an agreement of how someone or a group of people will be paid for their work'
    },
    'settlement machine': {
        'A settlement machine is a machine that settles disputes': 'a settlement machine is a machine that settles disputes'
    },
    'sugar people': {
        'people made of sugar': 'sugar people are people made of sugar'
    },
    'privatisation shock': {
        'A privatisation shock is a shock experinced by workers and employees of a government owned company when they hear that the company is going to be privatized.': 'a privatisation shock is a shock that is experinced by workers and employees of a government owned company when they hear that the company is going to be privatized'
    },
    'interference official': {
        'An interference official is an official that interferes.': 'an interference official is an official that interferes'
    },
    'lightning resistance': {
        'a lightning resistance is a resistance that takes place with lightning': 'a lightning resistance is a resistance that takes place with lightning'
    },
    'lightning collection': {
        'a lighting collection is a collection that contains lightning': 'a lighting collection is a collection that contains lightning'
    },
    'flight remedy': {
        'reroutes flights that had to change their original': 'a flight remedy is a remedy that reroutes flights that had to change their original flights'
    },
    'signing creations': {
        'creations of signing': 'signing creations are creations of signing'
    },
    'canine helmet': {
        'A canine helmet would probably be some sort of helmet for a dog, though I can\'t picture what this might look like or how it would work.': 'a canine helmet is a helmet for a dog',
        'A canine helmet is a helmet that is specifically designed for use by dogs.': 'a canine helmet is a helmet that is specifically designed for use by dogs'
    },
    'top world': {
        'A top world is a world that is the most idealistic of all imaginative worlds.': 'a top world is a world that is the most idealistic of all imaginative worlds'
    },
    'pet position': {
        'the place where a pet is, where it is sitting, or how it is sitting': 'a pet position is a position the place where a pet is, where it is sitting, or how it is sitting'
    },
    'operating prosecutor': {
        'An operating prosecutor is a prosecutor who currently takes the lead role in a legal proceeding': 'an operating prosecutor is a prosecutor who currently takes the lead role in a legal proceeding'
    },
    'engineering insurance': {
        'an engineering insurance is an insurance that covers engineering': 'an engineering insurance is an insurance that covers engineering'
    },
    'signing inventions': {
        'Signing inventions are inventions that improve or provide some alternate for something involving the act of signing': 'signing inventions are inventions that improve or provide some alternate for something involving the act of signing'
    },
    'role assembly': {
        'It is the organizational structure in some sort of process. So, each persons role makes up the role assembly.': 'a role assembly is the organizational structure in some sort of process, so, each person\'s role makes up the role assembly'
    }
}


def generate_phrase_context(compound):
    (modifier, head) = compound.split(' ')
    head_is_plural = bool(p.singular_noun(head))
    if head_is_plural:
        prefix_mod = modifier
        prefix_head = head
        prep = 'are'
    else:
        prefix_mod = p.a(modifier)
        prefix_head = p.a(head)
        prep = 'is'

    modifier_is_plural = bool(p.singular_noun(modifier))
    suffix_mod = modifier
    if not modifier_is_plural:
        plural_mod = p.plural_noun(modifier)
        if len(plural_mod) > len(modifier):
            prefix = plural_mod[:len(modifier)]
            suffix = plural_mod[len(modifier):]
            if prefix == modifier and (suffix == 's' or suffix == 'es'):
                suffix_mod = modifier + '(' + suffix + ')'

    # (prefix, suffix) pair
    return (prefix_mod + ' ' + head + ' ' + prep + ' ' + prefix_head, suffix_mod)


def clean_paraphrase(compound, paraphrase):
    if compound in final and paraphrase in final[compound]:
        return final[compound][paraphrase]

    if compound in pre_process and paraphrase in pre_process[compound]:
        paraphrase = pre_process[compound][paraphrase]

    # Need prefix and suffix for generating proper paraphrase
    (prefix, suffix) = generate_phrase_context(compound)

    def finalize(paraphrase):
        return prefix + ' ' + paraphrase + ' ' + suffix

    (modifier, head) = compound.split(' ')

    # Basic cleaning
    paraphrase = paraphrase.lower()
    paraphrase = paraphrase.replace('[', '').replace(']', '')

    # Remove anything before the entire compound
    # Ex) "An abbey assembly..."
    r1 = r'(?:' + modifier + r' )?' + head + ' (.*)'
    m = re.search(r1, paraphrase)
    if m:
        paraphrase = m.group(1).strip()
        m = re.search(r1, paraphrase)
        if m:
            paraphrase = m.group(1).strip()

    # If they led with a past participle, they forgot to add 'that is'
    if paraphrase.split(' ')[0][-2:] == 'ed':
        paraphrase = 'that is ' + paraphrase
    elif paraphrase.split(' ')[0][-2:] == 'es' or paraphrase.split(' ')[0][-1:] == 's':
        # Hack: it's probably a verb in the present tense -- should probably POS tag
        paraphrase = 'that ' + paraphrase

    # Account for paraphrases that are just a predicate
    # Ex) "office that controls"
    nomatch_preps = map(lambda s: '(?:' + s + ')', prepositions)
    predicates = r'((?:that)|(?:which)|(?:who)|(?:where)|(?:.*ing)|' + '|'.join(nomatch_preps) + ')'
    r2 = predicates + r'$'
    m = re.match(r2, paraphrase)
    if m:
        return finalize(m.group(0))

    # Remove anything before the predicate
    # Ex) "assembly that takes place at..."
    r3 = predicates + r' (.*)'
    m = re.search(r3, paraphrase)

    if m:
        paraphrase = m.group(0)
    else:
        paraphrase = 'that ' + paraphrase

    # Remove trailing punctuation
    r4 = r'(.*?)(?:\.|\?|\!)$'
    m = re.match(r4, paraphrase)
    if m:
        paraphrase = m.group(1)

    # Remove trailing modifier
    # Ex) "takes place at an abbey" or "take place at abbeys"
    modifier_forms = [
        modifier,
        modifier + '\(s\)',
        modifier + '\(es\)',
        modifier + 's'
    ]
    if p.plural_noun(modifier):
        modifier_forms.append(p.plural_noun(modifier))

    for form in modifier_forms:
        r5 = r'(.*?) ' + form + r'$'
        m = re.match(r5, paraphrase)
        if m:
            paraphrase = m.group(1)
            break

        # Account for modifier coming _before_ the end of the string
        r6 = r'(.*? ' + form + r' .*)'
        m = re.match(r6, paraphrase)
        if m:
            paraphrase = m.group(1)
            return prefix + ' ' + paraphrase

    return finalize(paraphrase)


if __name__ == '__main__':
    data = config.DATA(run=1)
    clean_data = {}
    for compound in data:
        clean_data[compound] = data[compound]
        for i, judgment in enumerate(data[compound]['judgments']):
            paraphrase = judgment['paraphrase']
            if paraphrase:
                print compound
                print paraphrase
                print clean_paraphrase(compound, paraphrase)
                print '---------------'
