import sys
import re
import json

with open(sys.argv[1], 'r') as fp:
    data = fp.read()

matches = re.findall(
    r'^(.*),(.*)\n--------------------------------------------\n(?:.*\n)?LSA-TASA: (.*)\n(?:.*\n)?LDA-TASA: (.*)\n', data, re.MULTILINE)

data = {}
for m in matches:
    if not m[1 - 1] in data:
        data[m[1 - 1]] = {}

    data[m[1 - 1]][m[2 - 1]] = {
        'lsa': m[3 - 1],
        'lda': m[4 - 1]
    }

print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
