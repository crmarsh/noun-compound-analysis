import json
import sys
import subprocess
from nltk.corpus import wordnet as wn
import config

if __name__ == '__main__':
    data = config.DATA(run=3)

    # Avoid re-classifying compounds that we've already analyzed
    compound_type_name = sys.argv[1].upper()
    compound_type = getattr(config, compound_type_name)

    best_synsets_for_head = {}
    best_synsets_for_modifier = {}

    # Wrap in try-catch to print results in spite of halting or errors
    counter = 0
    try:
        for compound in data:
            if data[compound]['score'][compound_type_name.title()] < 2 or compound in best_synsets_for_head:
                continue

            (modifier, head) = compound.split(' ')
            if modifier == 'operating':
                modifier = 'operation'
            mod_synsets = wn.synsets(modifier, pos=wn.NOUN)
            head_synsets = wn.synsets(head, pos=wn.NOUN)
            paraphrases = map(lambda x: x['paraphrase'], data[compound]['judgments'])

            # No need to ask if there's only one synset
            if len(mod_synsets) == 1 and len(head_synsets) == 1:
                synset_name = mod_synsets[0].name()
                best_mods = map(lambda p: synset_name if p != None else '', paraphrases)
                synset_name = head_synsets[0].name()
                best_heads = map(lambda p: synset_name if p != None else '', paraphrases)
            else:
                best_mods = []
                best_heads = []

                for paraphrase in paraphrases:
                    if paraphrase == None:
                        best_mods.append('')
                        best_heads.append('')
                    else:
                        print 'Compound: ' + compound + '\n'
                        print paraphrase
                        for i, synset in enumerate(mod_synsets):
                            print str(i) + '. ' + str(synset.name()) + ':', synset.definition(), '\n'
                        response = raw_input('Best modifier synset: ')
                        if not response:
                            response = mod_synsets[0].name()
                        else:
                            try:
                                response = mod_synsets[int(response)].name()
                            except:
                                pass
                        best_mods.append(response)

                        print paraphrase
                        for i, synset in enumerate(head_synsets):
                            print str(i) + '. ' + str(synset.name()) + ':', synset.definition(), '\n'
                        response = raw_input('Best head synset: ')
                        if not response:
                            response = head_synsets[0].name()
                        else:
                            try:
                                response = head_synsets[int(response)].name()
                            except:
                                pass

                        best_heads.append(response)

                        # Clear for maximum efficiency
                        subprocess.call('clear', shell=True)

            best_synsets_for_head[compound] = best_heads
            best_synsets_for_modifier[compound] = best_mods
    except:
        pass

    print json.dumps(best_synsets_for_head)
    print json.dumps(best_synsets_for_modifier)
