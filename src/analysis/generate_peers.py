from collections import defaultdict
from nltk.corpus import wordnet as wn
from util import ml
import config

attested = config.ATTESTED()


def compose(compound, peer_term):
    non_varied_term = compound.split(' ')[config.non_varied_index]
    if config.non_varied_index == 0:
        return non_varied_term + ' ' + peer_term
    else:
        return peer_term + ' ' + non_varied_term


def parse_synset(func):
    """Accepts either a string or a WordNet synset and returns the WordNet synset."""
    def use_synset(synset):
        if type(synset) is str or type(synset) is unicode:
            synset = wn.synset(synset)
        return func(synset)
    return use_synset


@parse_synset
def get_cohypernyms(synset):
    """Get the peers for a given synset, i.e., other synsets that share a hypernym."""
    cohypernyms = []
    for hypernym in synset.hypernyms():
        for child in hypernym.hyponyms():
            if child != synset:
                cohypernyms.append(child)
    return cohypernyms


@parse_synset
def get_hyponyms(synset):
    """Get the children for a given synset, i.e., its hyponyms."""
    return synset.hyponyms()


@parse_synset
def get_hypercohypernyms(synset):
    """Get the uncles for a given synset, i.e., cohypernym of its hypernyms."""
    all_uncles = [get_cohypernyms(hypernym) for hypernym in synset.hypernyms()]
    return [uncle for list_of_uncles in all_uncles for uncle in list_of_uncles]


@parse_synset
def get_hypocohypernyms(synset):
    """Get the nephews for a given synset, i.e., hyponyms of its cohypernyms."""
    all_nephews = [get_hyponyms(cohypernym) for cohypernym in get_cohypernyms(synset)]
    return [nephew for list_of_nephews in all_nephews for nephew in list_of_nephews]


@parse_synset
def get_lemmas(synset):
    words = []
    for lemma in synset.lemmas():
        w = lemma.name().split('.')[-1]
        if not '_' in w and not '-' in w and w.lower() == w:
            words.append(w)
    return words


def collect(compound, synset, generator):
    collected = []
    for h in generator(synset):
        for lemma in get_lemmas(h):
            if lemma in attested:
                collected.append({
                    'synset': h.name(),
                    'compound': compose(compound, lemma)
                })
    return collected


def get_siblings(compound, synset):
    return collect(compound, synset, get_cohypernyms)


def get_kids(compound, synset):
    return collect(compound, synset, get_hyponyms)


def get_uncles(compound, synset):
    return collect(compound, synset, get_hypercohypernyms)


def get_nephews(compound, synset):
    return collect(compound, synset, get_hypocohypernyms)


if __name__ == '__main__':
    import json

    data = config.DATA()
    # Collect peers
    siblings = defaultdict(lambda: defaultdict(list))
    kids = defaultdict(lambda: defaultdict(list))
    uncles = defaultdict(lambda: defaultdict(list))
    nephews = defaultdict(lambda: defaultdict(list))
    for compound_type in config.compound_types:
        synsets = config.SYNSET(compound_type)

        for compound in synsets:
            for i, synset_name in enumerate(synsets[compound]):
                # We only use judgments that agree with the majority for generating peers
                if data[compound]['judgments'][i]['rating'] != compound_type:
                    continue

                if not synset_name:
                    synset = ml.guess_synset(compound)
                    if not synset:
                        continue
                    else:
                        synset_name = synset.name()

                siblings[compound][synset_name] = get_siblings(compound, synset_name)
                kids[compound][synset_name] = get_kids(compound, synset_name)
                uncles[compound][synset_name] = get_uncles(compound, synset_name)
                nephews[compound][synset_name] = get_nephews(compound, synset_name)

    all_peers = {
        'siblings': siblings,
        'kids': kids,
        'uncles': uncles,
        'nephews': nephews
    }
    print json.dumps(all_peers, sort_keys=True, indent=4, separators=(',', ': '))
