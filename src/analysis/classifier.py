from collections import defaultdict, Counter
import numpy as np
from sklearn import svm, ensemble, neighbors, cross_validation, grid_search
from sklearn.metrics import precision_score, accuracy_score
from sklearn.preprocessing import normalize
from util import math
import loader


def train(n_folds=5, from_disk=False, against_attested=False, against_peers=False, cluster=False, majority_vote=False, group=False, guess=False):
    """
    group -- Should examples be evaluated as individual (generated, attested) pairs? Or should you
             perform a majority vote on all examples with the same (generated) component?
    cluster -- Should synsets be clustered before being considered as examples?
    load -- Should the model file be loaded or generated?
    """
    print "Parameters:", from_disk, against_attested, cluster, majority_vote, group, guess

    # Get data
    data_args = {
        'from_disk': from_disk,
        'majority_vote': majority_vote,
        'guess': guess,
        'cluster': cluster
    }
    (X, y, index) = loader.load_data(**data_args)

    print "Baseline:", math.baseline(y)

    if against_attested or against_peers:
        clf = ensemble.AdaBoostClassifier(
            n_estimators=100,
            learning_rate=1.0
        )

        if against_attested:
            (X_test, y_test) = loader.get_attested_data(cluster=cluster, from_disk=True)
        else:
            (X_test, y_test) = loader.get_peer_data(guess=guess, cluster=cluster, from_disk=False)

        # Scale data together
        X_all = np.concatenate((X, X_test), axis=0)
        X_all = normalize(X_all, norm='l2', axis=0, copy=True)
        X = X_all[:np.shape(X)[0]]
        X_test = X_all[np.shape(X)[0]:]

        clf.fit(X, y)
        y_pred = clf.predict(X_test)
        accuracy = accuracy_score(y_test, y_pred)
        precision = precision_score(y_test, y_pred, average=None)
        print "Against attested data:", accuracy, precision
        return
    else:
        X = normalize(X, norm='l2', axis=0, copy=True)

    # Should we classify by looking at all relevant examples for a generated compound?
    if group:
        indices_for_generated = defaultdict(list)
        for i, compound in enumerate(index):
            indices_for_generated[compound].append(i)

        num_examples = len(indices_for_generated)
    else:
        num_examples = len(X)

    # Run K-Fold cross validation
    kf = cross_validation.KFold(num_examples, n_folds=n_folds)

    accuracy_scores = []
    for train_index, test_index in kf:
        if group:
            generated_compounds = indices_for_generated.keys()
            X_train = None
            y_train = None

            # Generate training data
            for i in train_index:
                compound = generated_compounds[i]

                if X_train == None:
                    X_train = X[indices_for_generated[compound]]
                else:
                    X_train = np.vstack([X_train, X[indices_for_generated[compound]]])

                if y_train == None:
                    y_train = y[indices_for_generated[compound]]
                else:
                    y_train = np.append(y_train, y[indices_for_generated[compound]])

            # Generate testing data
            X_test = []
            y_test = []
            for i in test_index:
                compound = generated_compounds[i]
                X_test.append(X[indices_for_generated[compound]])

                # Label determined by majority vote
                counts = Counter(y[indices_for_generated[compound]])
                y_test.append(counts.most_common(1)[0][0])
        else:
            # Split into training and testing
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]

        # for weights in ['uniform', 'distance']:
        #     for k in range(1, 12):
        #         clf = neighbors.KNeighborsClassifier(k, weights=weights)
        #         clf.fit(X_train, y_train)

        #         if group:
        #             y_pred = []
        #             for i, groups_of_examples in enumerate(X_test):
        #                 predictions = defaultdict(int)
        #                 for example in groups_of_examples:
        #                     predictions[clf.predict(example)[0]] += 1
        #                 counts = Counter(predictions)
        #                 y_pred.append(counts.most_common(1)[0][0])
        #         else:
        #             y_pred = clf.predict(X_test)

        #         accuracy = accuracy_score(y_test, y_pred)
        #         precision = precision_score(y_test, y_pred, average=None)
        #         print weights, k, accuracy, precision
        #         accuracy_scores.append(accuracy)

        # svr = svm.SVC()
        # param_grid = [
        #     {'C': [1, 10, 100, 1000], 'kernel': ['sigmoid']},
        #     {'C': [1, 10, 100, 1000], 'gamma': [0.1, 0.01, 0.001, 0.0001], 'kernel': ['rbf']},
        # ]
        # clf = grid_search.GridSearchCV(svr, param_grid)

        for learning_rate in [0.5, 0.75, 1]:
            for n_estimators in [100]: #, 1000, 10000]:
                clf = ensemble.AdaBoostClassifier(
                    n_estimators=n_estimators,
                    learning_rate=learning_rate
                )
                y_pred = clf.fit(X_train, y_train).predict(X_test)
                accuracy = accuracy_score(y_test, y_pred)
                precision = precision_score(y_test, y_pred, average=None)
                print learning_rate, n_estimators, accuracy, precision
                accuracy_scores.append(accuracy)

    print "Average accuracy:", math.average(accuracy_scores)

if __name__ == '__main__':
    # parse arguments
    import argparse
    parser = argparse.ArgumentParser(description='Classify noun compounds as valid or invalid.')
    parser.add_argument('--n', type=int, default=8, help='Number of folds')
    parser.add_argument('--load', action='store_true')
    parser.add_argument('--attested', action='store_true')
    parser.add_argument('--peers', action='store_true')
    parser.add_argument('--cluster', action='store_true')
    parser.add_argument('--majority', action='store_true')
    parser.add_argument('--group', action='store_true')
    parser.add_argument('--guess', action='store_true')
    args = parser.parse_args()

    training_args = {
        'from_disk': args.load,
        'against_attested': args.attested,
        'against_peers': args.peers,
        'majority_vote': args.majority,
        'guess': args.guess,
        'cluster': args.cluster,
        'group': args.group,
        'n_folds': args.n
    }

    train(**training_args)
