from collections import Counter
import numpy as np


def baseline(y):
    counts = Counter(y).values()
    return 1.0 * max(counts) / sum(counts)


def average(x):
    if x:
        return 1.0 * sum(x) / len(x)
    return 0.0


def shuffle_in_unison(a, b, c, d):
    assert len(a) == len(b) and len(a) == len(c) and len(a) == len(d)
    p = np.random.permutation(len(a))
    return a[p], b[p], c[p], d[p]
