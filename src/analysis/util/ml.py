from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
import config

ic = wordnet_ic.ic('ic-brown.dat')
similarities = config.SIMILARITY()
generated_similarities = config.GENERATED_SIMILARITY()
#generated_features = config.FEATURES()

feature_names = [
    'shortest_path',
    'path_similarity',
    'lch_similarity',
    'wup_similarity',
    'res_similarity',
    'jcn_similarity',
    'lin_similarity',
    'lda',
    'lsa'
]


def to_key(s1, s2, c1, c2):
    if not (type(s1) is str or type(s1) is unicode):
        s1 = s1.name()
    if not (type(s2) is str or type(s2) is unicode):
        s2 = s2.name()

    return ','.join([s1, s2, c1, c2])


def label_for_type(compound_type):
    if compound_type == config.MAJOR:
        return 0
    elif compound_type == config.MINOR:
        return 1
    return 2


def compound_for_paraphrase(p):
    tokens = p.split(' ')
    offset = 1 if tokens[0] == 'a' or tokens[0] == 'an' else 0
    return tokens[offset + 0] + ' ' + tokens[offset + 1]


def guess_synset(compound):
    varied_word = compound.split(' ')[config.varied_index]
    try:
        return wn.synsets(varied_word, pos=wn.NOUN)[0]
    except:
        return None


def get_synset(synset_name, compound, guess=True):
    # Unfortunately, we can't use non-noun synsets for feature calculation
    if '.v.' in synset_name or '.s.' in synset_name:
        return None
    elif synset_name:
        return wn.synset(synset_name)
    else:
        # If there was no paraphrase provided, we can guess the synset
        synset = None
        if guess:
            synset = guess_synset(compound)
        return synset


def get_attested_synsets(compound, variants, include_compounds=False):
    attested_synsets = []

    if include_compounds:
        attested_compounds = []

    for variant in variants:
        best_guess = guess_synset(variant)
        if best_guess and variant != compound:
            attested_synsets.append(best_guess)
            if include_compounds:
                attested_compounds.append(variant)

    if include_compounds:
        return (attested_synsets, attested_compounds)
    return attested_synsets


def feature_vector(synset, attested_synset, compound, variant):
    # See if vector has been saved to disk
    # key = to_key(synset, attested_synset, compound, variant)
    # if key in generated_features:
    #     return generated_features[key]

    # Otherwise, compute
    if type(synset) is str or type(synset) is unicode:
        synset = wn.synset(synset)
    if type(attested_synset) is str or type(attested_synset) is unicode:
        attested_synset = wn.synset(attested_synset)

    u = compound.split(' ')[config.varied_index]
    v = variant.split(' ')[config.varied_index]

    features = [
        lambda s, t: s.shortest_path_distance(t),
        wn.path_similarity,
        wn.lch_similarity,
        wn.wup_similarity,
        lambda s, t: wn.res_similarity(s, t, ic),
        lambda s, t: wn.jcn_similarity(s, t, ic),
        lambda s, t: wn.lin_similarity(s, t, ic),
    ]

    try:
        similarity_scores = [similarities[u][v]['lda'], similarities[u][v]['lsa']]
    except:
        similarity_scores = [
            generated_similarities[u][v]['lda'], generated_similarities[u][v]['lsa']]
    return [f(synset, attested_synset) for f in features] + similarity_scores


def get_label(scores, judgment, majority_vote=False):
    if majority_vote:
        for judgment_type in scores:
            if scores[judgment_type] >= 2:
                return label_for_type(getattr(config, judgment_type.upper()))
    else:
        return label_for_type(getattr(config, judgment))
