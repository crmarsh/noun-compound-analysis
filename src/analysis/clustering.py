import sys
from sklearn.cluster import AffinityPropagation
from nltk.corpus import wordnet as wn
import numpy as np
import config


def get_coordinate(synsets):
    parent = synsets[0]
    for s in synsets[1:]:
        parent = parent.lowest_common_hypernyms(s)[0]


def cluster(synsets):
    """Cluster words using the AffinityPropagation algorithm."""
    X = np.matrix([[wn.path_similarity(i, j) for j in synsets] for i in synsets])
    af = AffinityPropagation(affinity='precomputed').fit(X)
    cluster_centers_indices = af.cluster_centers_indices_
    labels = af.labels_
    n_clusters_ = len(cluster_centers_indices)
    return n_clusters_, labels, af


def get_similar(attested_synsets, new_synset):
    wn_synsets = attested_synsets + [new_synset]
    (clusters, labels, af) = cluster(wn_synsets)
    return [s for (i, s) in enumerate(attested_synsets) if labels[i] == labels[-1]]

if __name__ == '__main__':
    compound_type = getattr(config, sys.argv[1].upper())
    # Load data
    variants = config.VARIANTS(compound_type)
    synsets = config.SYNSET(compound_type)

    # Unique synsets
    for compound in synsets.keys():
        synsets[compound] = set(synsets[compound])

    # Cluster on each head
    compounds = variants.keys()
    for compound in compounds:
        for synset_name in synsets[compound]:
            # 'Major difficulty' has no assigned synset; also skip verbs
            if not synset_name or '.v.' in synset_name or '.s.' in synset_name:
                continue

            synset = wn.synset(synset_name)

            variant_synsets = []
            for variant in variants[compound]:
                modifier = variant.split(' ')[0]

                # Skip if no synset exists for the modifier
                try:
                    variant_synset = wn.synsets(modifier, pos=wn.NOUN)[0]
                except:
                    continue

                variant_synsets.append(variant_synset)

            (clusters, labels, af) = cluster(variant_synsets + [synset])
            print variant_synsets + [synset]
            print clusters, labels

            break
        break
