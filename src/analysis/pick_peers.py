from random import choice
from util.ml import label_for_type
import config

generated = config.GENERATED()
relations = { k: 0 for k in generated }
for compound_type in config.compound_types:

    label = label_for_type(compound_type)
    synsets = config.SYNSET(compound_type)
    variants = config.VARIANTS(compound_type)

    added = False
    for compound in synsets:
        for relationship in sorted(relations, key=relations.get):
            peers = generated[relationship]
            if not compound in peers:
                continue

            for synset_name in set(synsets[compound]):
                if synset_name in peers[compound] and peers[compound][synset_name]:
                    c = choice(peers[compound][synset_name])

                    dupe = len(peers[compound][synset_name]) > 1
                    while dupe:
                        dupe = False
                        for other_relations in relations:
                            if other_relations == relationship:
                                continue
                            for x in generated[other_relations][compound][synset_name]:
                                if x['compound'] == c['compound']:
                                    c = choice(peers[compound][synset_name])
                                    dupe = True

                    added = True
                    relations[relationship] += 1
                    print c['compound']
                    break

            if added:
                break

        # if not added:
        #     print compound
        added = False

print relations
