import sys
from util import math
import config

if __name__ == '__main__':
    compound_type = getattr(config, sys.argv[1].upper())

    # Load distances
    distances = config.DISTANCES(compound_type, run=2)
    path_distances = config.PATH_DISTANCES(compound_type, run=2)
    lch_distances = config.LCH_DISTANCES(compound_type, run=2)

    distance_averages = []
    path_averages = []
    lch_averages = []
    for compound in distances:
        for synset_name in distances[compound]:
            synset_to_synset_distances = distances[compound][synset_name].values()
            # Remove null values
            synset_to_synset_distances = filter(bool, synset_to_synset_distances)

            # Compute average distance
            if len(synset_to_synset_distances) > 0:
                distance_averages.append(math.average(synset_to_synset_distances))

            synset_to_synset_distances = path_distances[compound][synset_name].values()
            # Remove null values
            synset_to_synset_distances = filter(bool, synset_to_synset_distances)

            # Compute average distance
            if len(synset_to_synset_distances) > 0:
                path_averages.append(math.average(synset_to_synset_distances))

            synset_to_synset_distances = lch_distances[compound][synset_name].values()
            # Remove null values
            synset_to_synset_distances = filter(bool, synset_to_synset_distances)

            # Compute average distance
            if len(synset_to_synset_distances) > 0:
                lch_averages.append(math.average(synset_to_synset_distances))

    print math.average(distance_averages), math.average(path_averages), math.average(lch_averages)
