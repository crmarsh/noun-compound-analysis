import sys

import config


if __name__ == '__main__':
    compound_type = getattr(config, sys.argv[1].upper())
    synsets = config.SYNSET(compound_type, run=1)

    s = []
    for compound in synsets:
        vs = synsets[compound]
        n = len(set(filter(bool, synsets[compound])))
        m = len(filter(bool, synsets[compound]))
        s.append(1.0 * n / m)
        # number of distinct senses / number of judgments
    #s = [len(set(filter(bool, synsets[compound]))) for compound in synsets]

    t = [compound for compound in synsets if len(set(filter(bool, synsets[compound]))) == 1]
    print 'Average num. senses per compound:', sum(s) / float(len(s))
    print 'Pct. of compounds with a single sense:', len(t) / float(len(s))
