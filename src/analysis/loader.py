import pickle
import sys
from nltk.corpus import wordnet as wn
import numpy as np
import clustering
from util import math, ml
import config


def get_attested_data(from_disk=False, cluster=False):
    if from_disk:
        X = np.load(config.FEATURE_ATTESTED_MODEL_PATH)
        y = np.load(config.LABEL_ATTESTED_MODEL_PATH)
        return (X, y)

    X = []
    y = []

    for compound_type in config.compound_types:
        variants = config.VARIANTS(compound_type)
        for root_compound in variants:
            attested_synsets = ml.get_attested_synsets(root_compound, variants[root_compound])

            if cluster:
                (_, labels, _) = clustering.cluster(attested_synsets)

            n = len(attested_synsets)
            for i in range(n):
                for j in range(i + 1, n):
                    if cluster and labels[i] != labels[j]:
                        continue

                    features = ml.feature_vector(attested_synsets[i], attested_synsets[j])
                    X.append(features)
                    y.append(ml.label_for_type(config.NONE))

    X = np.array(X)
    y = np.array(y)

    # Write to disk
    np.save(config.FEATURE_ATTESTED_MODEL_PATH, X)
    np.save(config.LABEL_ATTESTED_MODEL_PATH, y)

    return (X, y)


def get_peer_data(from_disk=False, guess=True, cluster=False):
    if from_disk:
        X = np.load(config.FEATURE_PEER_MODEL_PATH)
        y = np.load(config.LABEL_PEER_MODEL_PATH)
        return (X, y)

    X = []
    y = []

    generated = config.GENERATED()

    for compound_type in config.compound_types:
        label = ml.label_for_type(compound_type)
        # Won't have any agreeable paraphrases
        if not guess and compound_type == config.MAJOR:
            continue

        synsets = config.SYNSET(compound_type)

        for compound in synsets:
            # Get attested synsets
            attested_synsets = ml.get_attested_synsets(compound, synsets[compound])
            for synset_name in synsets[compound]:
                if synset_name in generated[compound]:
                    # Get peers
                    for peer in generated[compound][synset_name]:
                        synset = wn.synset(peer['synset'])

                        if cluster:
                            clusters = clustering.get_similar(attested_synsets, synset)

                        for i, attested_synset in enumerate(attested_synsets):
                            # If we clustered but these synsets aren't in the same clusters, ignore
                            if cluster and not attested_synset in clusters:
                                continue

                            X.append(ml.feature_vector(synset, attested_synset, None, None))
                            y.append(label)

    X = np.array(X)
    y = np.array(y)

    # Write to disk
    np.save(config.FEATURE_PEER_MODEL_PATH, X)
    np.save(config.LABEL_PEER_MODEL_PATH, y)

    return (X, y)


def load_data(from_disk=False, cluster=False, majority_vote=False, guess=True, extra=True):
    # Load data from disk, if enabled
    if from_disk:
        X = np.load(config.FEATURE_MODEL_PATH)
        y = np.load(config.LABEL_MODEL_PATH)
        with open(config.INDEX_MODEL_PATH, 'r') as fp:
            index = pickle.load(fp)
        return (X, y, index)

    # Else, recompute
    data = config.DATA()

    def get_examples_for_type(compound_type):
        synsets = config.SYNSET(compound_type)
        variants = config.VARIANTS(compound_type)

        X = []
        y = []
        index = []
        attested_index = []

        for compound in synsets:
            # If we couldn't find any attested variants, ignore
            if not compound in variants:
                continue

            # Gather attested variants
            (attested_synsets, attested_compounds) = ml.get_attested_synsets(
                compound, variants[compound], include_compounds=True
            )

            for i, synset_name in enumerate(synsets[compound]):
                synset = ml.get_synset(synset_name, compound, guess=guess)
                if not synset:
                    continue

                if cluster:
                    clusters = clustering.get_similar(attested_synsets, synset)

                label = ml.get_label(
                    data[compound]['score'],
                    data[compound]['judgments'][i]['rating'].upper(),
                    majority_vote=majority_vote
                )

                for j, attested_synset in enumerate(attested_synsets):
                    # If we clustered but these synsets aren't in the same clusters, ignore
                    if cluster and not attested_synset in clusters:
                        continue

                    variant = attested_compounds[j]
                    vector = ml.feature_vector(synset, attested_synset, compound, variant)
                    if not extra:
                        vector = vector[:-2]
                    for i, k in enumerate(vector):
                        if k >= 1.0e+300:
                            vector[i] = sys.maxint
                    X.append(vector)
                    y.append(label)
                    index.append(compound)
                    attested_index.append(variant)

        return (X, y, index, attested_index)

    # Sum over compound types
    X = []
    y = []
    index = []
    attested_index = []
    for compound_type in config.compound_types:
        (X_i, y_i, index_i, attested_index_i) = get_examples_for_type(compound_type)
        X += X_i
        y += y_i
        index += index_i
        attested_index += attested_index_i

    # Shuffle and normalize
    (X, y, index, attested_index) = math.shuffle_in_unison(np.array(X), np.array(y), np.array(index), np.array(attested_index))
    (X, y, index, attested_index) = (X, y, list(index), list(attested_index))

    # Write to disk
    np.save(config.FEATURE_MODEL_PATH, X)
    np.save(config.LABEL_MODEL_PATH, y)
    with open(config.INDEX_MODEL_PATH, 'wb') as fp:
        pickle.dump(index, fp)

    return (X, y, index, attested_index)
