import argparse
import operator
import random

import config
import compound

attested = [compound.Compound(c) for c in config.ATTESTED()]


def generate_ternary(compound, is_left):
    """
    Generate a ternary compound containing `compound` as a sub-compound. If `is_left` is True,
    `compound` will be used as the left branch. Otherwise, it will be the right branch.
    """
    if is_left:
        shared = 'head'
        shared_attested = 'modifier'
    else:
        shared = 'modifier'
        shared_attested = 'head'

    candidates = set([])
    for attested_compound in attested:
        if getattr(compound, shared) == getattr(attested_compound, shared_attested):
            candidates.add(attested_compound)

    candidates = list(candidates)
    random.shuffle(candidates)
    for to_merge in candidates:
        if (is_left and to_merge.head == compound.modifier) or (not is_left and to_merge.modifier == compound.head):
            continue

        if is_left:
            return '{' + str(compound) + '} ' + to_merge.head
        else:
            return to_merge.modifier + ' {' + str(compound) + '}'
    return None


def generate(num_left, num_right, label=None):
    # Pick set of root ocmpounds
    if label:
        compounds = config.SYNSET(prefix=label, run=1).keys()
    else:
        compounds = config.ALL_SYNSET(run=1).keys()

    left_counts = {}
    right_counts = {}
    for c in compounds:
        left_counts[c] = 0
        right_counts[c] = 0

    # Generate num_left left-rooted compounds and num_right right-rooted
    ternary = set([])
    exhausted = set([])
    while len(ternary) < num_left + num_right:
        is_left = len(ternary) < num_left
        relevant_dict = left_counts if is_left else right_counts
        # Each time, sort by number of times the compound has been included so that we
        # get a decent distribution (stil some randomness)
        for (root, _) in sorted(relevant_dict.items(), key=operator.itemgetter(1)):
            c = compound.Compound(root)
            if (c, is_left) in exhausted:
                continue

            generated = generate_ternary(c, is_left)
            if generated and not generated in ternary:
                ternary.add(generated)
                relevant_dict[root] += 1
                break

            # If we failed to find anything for this compound, we won't find any in the future,
            # so we simply skip
            exhausted.add((c, is_left))
    print 'Left counts:'
    print left_counts
    print 'Right counts:'
    print right_counts
    assert(sum(left_counts.values()) + sum(right_counts.values()) == num_left + num_right)
    return ternary

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate a set of ternary compounds.')
    parser.add_argument(
        'num_left', type=int, default=5, help='Number of left-rooted compounds to generate.')
    parser.add_argument(
        'num_right', type=int, default=5, help='Number of right-rooted compounds to generate.')
    parser.add_argument('--label', help='Label of root compounds.')
    args = parser.parse_args()

    # Generate ternary
    for c in generate(args.num_left, args.num_right, label=args.label):
        print c
