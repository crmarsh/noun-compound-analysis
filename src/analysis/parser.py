import tempfile
import os
import subprocess

PARSER_PATH = '/Users/crmarsh/Desktop/COS_497/noun-compounds/third-party/stanford-parser-full-2014-10-31/lexparser.sh'


def _get_sd_representations(output):
    sds = []
    sd = []
    in_sd = False
    for line in output.split('\n'):
        # Iterate until you see the first blank line
        if not line:
            in_sd = not in_sd
            if not in_sd:
                sds.append(sd)
                sd = []
        elif in_sd:
            sd.append(line)
    return sds


def parse(paraphrase):
    return parse_many([paraphrase])[0]


def parse_many(paraphrases):
    # Create temp file
    fp = tempfile.NamedTemporaryFile(delete=False)
    for paraphrase in paraphrases:
        fp.write(paraphrase + '.\n')
    fp.close()

    output = subprocess.check_output([PARSER_PATH, fp.name])

    # Delete temp file
    os.unlink(fp.name)

    # Parse output
    return _get_sd_representations(output)


if __name__ == '__main__':
    import sys
    print parse(sys.argv[1])
