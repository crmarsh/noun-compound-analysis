import config

if __name__ == '__main__':
    data = config.DATA()
    num_min = 0
    for compound_type in [config.NONE, config.MINOR, config.MAJOR]:
        variants = config.VARIANTS(compound_type)

        # Go from 'Major' to 'Eccentric'
        if compound_type == config.MAJOR:
            for compound in variants.keys():
                if data[compound]['score']['Major'] > 2:
                    del variants[compound]

        max_variants = max(len(v) for v in variants.values())
        min_variants = min(len(v) for v in variants.values())
        total_variants = sum(len(v) for v in variants.values())
        avg_variants = total_variants / float(len(variants))
        print 'Compound type:', compound_type, '(' + str(len(variants)) + ')'
        print 'Avg. number of variants:', avg_variants
        for c in variants:
            if len(variants[c]) == max_variants:
                print 'Max:', c, 'with', max_variants
            if len(variants[c]) == min_variants:
                num_min += 1
    print 'Total number of 1-variant compounds:', num_min
