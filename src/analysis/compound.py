class Compound(object):

    def __init__(self, compound):
        (self.modifier, self.head) = compound.split(' ')

    def __str__(self):
        return self.modifier + ' ' + self.head

    def __repr__(self):
        return 'Compound(head="' + self.head + '", modifier="' + self.modifier + '")'

    def __eq__(self, other):
        return self.head == other.head and self.modifier == other.modifier
