from collections import defaultdict
import config

generated_peers = config.GENERATED()
counters = defaultdict(int)

initial_data = config.DATA()
data = config.DATA(run=4)

for line in data:
    if line in initial_data:
        continue
    found = False
    #line = line.strip()
    for relation in generated_peers:
        for compound in generated_peers[relation]:
            for synset in generated_peers[relation][compound]:
                for peer in generated_peers[relation][compound][synset]:
                    if line == peer['compound']:
                        counters[relation] += 1
                        found = True
                        break
                    if found:
                        break
                if found:
                    break
            if found:
                break
        if found:
            break

print counters
