import collections
import json
import sys

import config


def _get_head(compound):
    return compound.split(' ')[1]


def _get_modifier(compound):
    return compound.split(' ')[0]


def _get_variant_component(compound):
    if config.SIMILARITY_TYPE == 'head':
        return _get_head(compound)
    else:
        return _get_modifier(compound)


if __name__ == '__main__':
    attested = []
    with open(config.ATTESTED_PATH, 'r') as fp:
        for line in fp:
            attested.append(line.strip())

    compound_type = getattr(config, sys.argv[1].upper())
    synsets = config.SYNSET(compound_type, run=4)

    same = collections.defaultdict(list)
    for compound in synsets:
        component = _get_variant_component(compound)
        for attested_compound in attested:
            attested_component = _get_variant_component(attested_compound)
            if component == attested_component:
                same[compound].append(attested_compound)

    print json.dumps(same)
