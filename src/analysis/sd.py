import parser
import cleaner
import config


def _strip_contents(sd):
    return map(lambda x: x.split('(')[0], sd)


def paraphrases_as_sds():
    """Compute dependency representations of paraphrases and store them in data."""
    data = config.DATA(run=4)

    paraphrases = []
    for compound in data:
        for judgment in data[compound]['judgments']:
            paraphrase = judgment['paraphrase']
            if paraphrase:
                paraphrases.append(
                    cleaner.clean_paraphrase(compound, paraphrase)
                )

    sds = parser.parse_many(paraphrases)
    sds = map(_strip_contents, sds)

    # Now, modify data
    i = 0
    for compound in data:
        for j, judgment in enumerate(data[compound]['judgments']):
            paraphrase = judgment['paraphrase']
            if paraphrase:
                data[compound]['judgments'][j]['paraphrase'] = paraphrases[i]
                data[compound]['judgments'][j]['sd'] = sds[i]
                i += 1

    return data


def load_sds(include_paraphrases=False):
    """
    Load dependency representations from disk. Can also zip them with paraphrases for comparsion.
    """
    data = config.CLEAN_DATA()

    sds = []
    if include_paraphrases:
        paraphrases = []
    for compound in data:
        for judgment in data[compound]['judgments']:
            if 'sd' in judgment:
                if include_paraphrases:
                    paraphrases.append(judgment['paraphrase'])
                sds.append(judgment['sd'])

    if include_paraphrases:
        return (paraphrases, sds)

    return sds

if __name__ == '__main__':
    import json
    print json.dumps(paraphrases_as_sds(), sort_keys=True, indent=4, separators=(',', ': '))
