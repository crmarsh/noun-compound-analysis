import json
import sys
import collections
import config
from nltk.corpus import wordnet as wn

if __name__ == '__main__':
    compound_type = getattr(config, sys.argv[1].upper())
    # Load data
    variants = config.VARIANTS(compound_type, run=1)
    synsets = config.SYNSET(compound_type, run=1)
    # Unique synsets
    for compound in synsets.keys():
        synsets[compound] = set(synsets[compound])

    # Store distances between synsets
    distances = collections.defaultdict(lambda: collections.defaultdict(dict))
    path_distances = collections.defaultdict(lambda: collections.defaultdict(dict))
    lch_distances = collections.defaultdict(lambda: collections.defaultdict(dict))

    compounds = variants.keys()
    for compound in compounds:
        for synset_name in synsets[compound]:
            # 'Major difficulty' has no assigned synset; also skip verbs
            if not synset_name or '.v.' in synset_name or '.s.' in synset_name:
                continue

            synset = wn.synset(synset_name)

            for variant in variants[compound]:
                varied = variant.split(' ')[config.varied_index]

                # Skip if no synset exists for the head
                try:
                    variant_synset = wn.synsets(varied, pos=wn.NOUN)[0]
                except:
                    continue

                variant_synset_name = variant_synset.name()
                distances[compound][synset_name][
                    variant_synset_name] = synset.shortest_path_distance(variant_synset)
                path_distances[compound][synset_name][
                    variant_synset_name] = synset.path_similarity(variant_synset)
                lch_distances[compound][synset_name][
                    variant_synset_name] = synset.lch_similarity(variant_synset)

    with open(config.DISTANCES_PATH(compound_type, run=1), 'w+') as fp:
        json.dump(distances, fp, sort_keys=True, indent=4, separators=(',', ': '))
    with open(config.PATH_DISTANCES_PATH(compound_type, run=1), 'w+') as fp:
        json.dump(path_distances, fp, sort_keys=True, indent=4, separators=(',', ': '))
    with open(config.LCH_DISTANCES_PATH(compound_type, run=1), 'w+') as fp:
        json.dump(lch_distances, fp, sort_keys=True, indent=4, separators=(',', ': '))
