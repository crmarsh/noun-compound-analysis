import argparse
import collections
import logging
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics.pairwise import linear_kernel
from sklearn.metrics import silhouette_score
from sklearn.cluster import KMeans
from gensim import corpora, models
import lda
import numpy as np

import config
from util import ml
import sd

logging.getLogger('lda').setLevel(logging.ERROR)
logging.getLogger('gensim').setLevel(logging.ERROR)


def _to_word_vec(sd):
    return ' '.join(sd)


def invert(value_to_index):
    """Invert a dictionary of {key, index} pairs."""
    a = []
    for i in range(len(value_to_index)):
        for value in value_to_index:
            if value_to_index[value] == i:
                a.append(value)
                break
    return a


class Model(object):

    def __init__(self, n_clusters=8, n_topics=5):
        (self.paraphrases, self.sds) = sd.load_sds(include_paraphrases=True)
        self.vectors = map(_to_word_vec, self.sds)

        # TF-IDF
        self.tfidfModel = TfidfVectorizer().fit(self.vectors)
        self.matrix = self.tfidfModel.transform(self.vectors)

        # LDA
        vectorizer = CountVectorizer().fit(self.vectors)
        self._vocab = invert(vectorizer.vocabulary_)
        counts = vectorizer.transform(self.vectors)
        self.ldaModel = lda.LDA(n_topics=n_topics, n_iter=1000, random_state=1).fit(counts)

        # Create dictionary
        # dictionary = corpora.Dictionary(self.sds)
        # corpus = [dictionary.doc2bow(doc) for doc in self.sds]
        # hdp = models.HdpModel(corpus, dictionary, T=300, K=20, chunksize=10, tau=25.0)
        # print hdp.show_topics(topics=-1, topn=1, formatted=True)

        # KMeans
        self.kmeans = KMeans(n_clusters=n_clusters).fit(self.matrix)
        self.clusterLabels = self.kmeans.labels_

    def paraphrase(self, i):
        return self.paraphrases[i]

    def sd(self, i):
        return self.sd(i)

    def similarity(self, sd1, sd2):
        word_vectors = map(_to_word_vec, [sd1, sd2])
        (v1, v2) = self.tfidfModel.transform(word_vectors)
        return float(linear_kernel(v1, v2))

    def get_most_similar(self, idx, top=5):
        """Get the indices (in self.paraphrases) of the paraphrases closest to that at idx."""
        cosine_similarities = linear_kernel(self.matrix[idx:idx + 1], self.matrix).flatten()
        related_docs_indices = cosine_similarities.argsort()[:-top:-1]
        return related_docs_indices

    def get_cluster(self, idx):
        """Get the cluster for paraphrase at `idx`."""
        return self.clusterLabels[idx]

    def get_paraphrases_for_cluster(self, idx):
        """Return all the paraphrases whose cluster is cluster `idx`."""
        return [p for (i, p) in enumerate(self.paraphrases) if self.get_cluster(i) == idx]

    def get_topic(self, idx):
        """Get the most influential topic for paraphrase at `idx`."""
        return self.ldaModel.doc_topic_[idx].argmax()

    def get_topic_distribution(self, idx, n_top_words=5):
        """Get the words that comprise topic `idx`, sorted in descending order by importance."""
        topic_dist = self.ldaModel.topic_word_[idx]
        topic_words = np.array(self._vocab)[np.argsort(topic_dist)][:-n_top_words:-1]
        return topic_words

    def get_paraphrases_for_topic(self, idx):
        """Return all the paraphrases whose most influential topic is topic `idx`."""
        influence = [(self.ldaModel.doc_topic_[i][idx], p, i) for i, p in enumerate(self.paraphrases)]
        sorted_paraphrases = [(p, i) for (inf, p, i) in sorted(influence, reverse=True)]
        return [p for (p, i) in sorted_paraphrases if self.get_topic(i) == idx]

if __name__ == '__main__':
    np.random.seed(seed=0)
    parser = argparse.ArgumentParser(description='Cluster paraphrases.')
    parser.add_argument('--n_clusters', type=int, default=5, help='Number of clusters')
    parser.add_argument('--n_topics', type=int, default=5, help='Number of topics')
    parser.add_argument('--lda', action='store_true', help='Run LDA algorithm')
    parser.add_argument('--cluster', action='store_true', help='Run k-means clustering')
    args = parser.parse_args()

    # Run model
    model = Model(n_clusters=args.n_clusters, n_topics=args.n_topics)

    # Print requested information
    if args.lda:
        data = config.CLEAN_DATA()
        counter = collections.defaultdict(lambda: collections.defaultdict(int))
        totals = collections.defaultdict(int)
        for i in range(args.n_topics):
            print "Topic " + str(i) + ":"
            print model.get_topic_distribution(i, n_top_words=15)
            for paraphrase in model.get_paraphrases_for_topic(i)[:10]:
                print paraphrase
                compound = ml.compound_for_paraphrase(paraphrase)
                for judgment in data[compound]['judgments']:
                    if judgment['paraphrase'] == paraphrase:
                        counter[i][judgment['rating']] += 1
                        totals[judgment['rating']] += 1

                        if data[compound]['score']['Major'] == 2:
                            counter[i]['Eccentric'] += 1
                            totals['Eccentric'] += 1
            print "---------------------------"

        for topic_id in counter:
            print 'Topic', topic_id
            for score_type in counter[topic_id]:
                denom = sum(counter[topic_id].values()) - counter[topic_id]['Eccentric']
                print '%s: %f' % (score_type, float(counter[topic_id][score_type]) / denom)
            print ''

    if args.cluster:
        data = config.CLEAN_DATA()
        counter = collections.defaultdict(lambda: collections.defaultdict(int))
        for i in range(args.n_clusters):
            print "Cluster " + str(i) + ":"
            for paraphrase in model.get_paraphrases_for_cluster(i):
                print paraphrase

                compound = ml.compound_for_paraphrase(paraphrase)
                for judgment in data[compound]['judgments']:
                    if judgment['paraphrase'] == paraphrase:

                        if data[compound]['score']['Major'] == 2:
                            counter[i]['Eccentric'] += 1
                        else:
                            counter[i][judgment['rating']] += 1

            print "---------------------------"

        totals = collections.defaultdict(int)
        strings = collections.defaultdict(str)
        for cluster_id in counter:
            for score_type in counter[cluster_id]:
                totals[score_type] += counter[cluster_id][score_type]
        for cluster_id in counter:
            print 'Cluster', cluster_id
            denom = sum(counter[cluster_id].values()) # - counter[cluster_id]['Eccentric']
            for score_type in counter[cluster_id]:
                norm_count = float(counter[cluster_id][score_type]) / denom
                print '%s: %f' % (score_type, norm_count)
                strings[score_type] += ' (' + str(cluster_id) + ',' + str(norm_count) + ')'
            print ''
        print strings

