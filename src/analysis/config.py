import json
from util.general import merge

RUN = 'run1'
SIMILARITY_TYPE = 'head'

DATA_DIR = '../../data/mturk'
TERNARY_PATH = DATA_DIR + '/ternary1/main.json'
ATTESTED_PATH = '../../data/datasets/all_compounds.txt'
SIMILARITY_PATH = RUN + '/' + SIMILARITY_TYPE + '/' + 'similarity.json'
GENERATED_PATH = RUN + '/' + SIMILARITY_TYPE + '/' + 'generated.json'
GENERATED_SIMILARITY_PATH = RUN + '/' + SIMILARITY_TYPE + '/' + 'similarity_generated.json'
FEATURES_PATH = RUN + '/' + SIMILARITY_TYPE + '/' + 'similarity_generated_features.json'
MODEL_PATH = './model'
FEATURE_MODEL_PATH = MODEL_PATH + '/' + 'feature-model.npy'
LABEL_MODEL_PATH = MODEL_PATH + '/' + 'label-model.npy'
INDEX_MODEL_PATH = MODEL_PATH + '/' + 'index-model.pkl'
ATTESTED_MODEL_PATH = MODEL_PATH + '/' + 'attested'
PEER_MODEL_PATH = MODEL_PATH + '/' + 'peer'
FEATURE_ATTESTED_MODEL_PATH = ATTESTED_MODEL_PATH + '/' + 'feature-model.npy'
LABEL_ATTESTED_MODEL_PATH = ATTESTED_MODEL_PATH + '/' + 'label-model.npy'
FEATURE_PEER_MODEL_PATH = PEER_MODEL_PATH + '/' + 'feature-model.npy'
LABEL_PEER_MODEL_PATH = PEER_MODEL_PATH + '/' + 'label-model.npy'

NONE = 'None'
MINOR = 'Minor'
MAJOR = 'Major'

compound_types = [NONE, MINOR, MAJOR]

varied_index = ['head', 'modifier'].index(SIMILARITY_TYPE)
non_varied_index = ['modifier', 'head'].index(SIMILARITY_TYPE)


# Path generators
def DATA_PATH(run=1):
    return DATA_DIR + '/run' + str(run) + '/' + 'main.json'


def CLEAN_DATA_PATH(run=1):
    return DATA_DIR + '/run' + str(run) + '/' + 'main.clean.json'


def SYNSET_PATH(prefix, run=1):
    return './run' + str(run) + '/' + SIMILARITY_TYPE + '/' + prefix + '/synset.json'


def VARIANTS_PATH(prefix, run=1):
    return './run' + str(run) + '/' + SIMILARITY_TYPE + '/' + prefix + '/same.json'


def DISTANCES_PATH(prefix, run=1):
    return './run' + str(run) + '/' + SIMILARITY_TYPE + '/' + prefix + '/distances.json'


def LCH_DISTANCES_PATH(prefix, run=1):
    return './run' + str(run) + '/' + SIMILARITY_TYPE + '/' + prefix + '/lch_distances.json'


def PATH_DISTANCES_PATH(prefix, run=1):
    return './run' + str(run) + '/' + SIMILARITY_TYPE + '/' + prefix + '/path_distances.json'


# JSON loaders
def SYNSET(prefix, run=1):
    with open(SYNSET_PATH(prefix, run=run), 'r') as fp:
        return json.load(fp)


def ALL_SYNSET(run=1):
    data = {}
    for compound in compound_types:
        data.update(SYNSET(compound, run=run))
    return data


def VARIANTS(prefix, run=1):
    with open(VARIANTS_PATH(prefix, run=run), 'r') as fp:
        return json.load(fp)


def ALL_VARIANTS(run=1):
    data = {}
    for compound in compound_types:
        data.update(VARIANTS(compound, run=run))
    return data


def DISTANCES(prefix, run=1):
    with open(DISTANCES_PATH(prefix, run=run), 'r') as fp:
        return json.load(fp)


def ALL_DISTANCES(run=1):
    data = {}
    for compound in compound_types:
        data.update(DISTANCES(compound, run=run))
    return data


def LCH_DISTANCES(prefix, run=1):
    with open(LCH_DISTANCES_PATH(prefix, run=run), 'r') as fp:
        return json.load(fp)


def ALL_LCH_DISTANCES(run=1):
    data = {}
    for compound in compound_types:
        data.update(LCH_DISTANCES(compound, run=run))
    return data


def PATH_DISTANCES(prefix, run=1):
    with open(PATH_DISTANCES_PATH(prefix, run=run), 'r') as fp:
        return json.load(fp)


def ALL_PATH_DISTANCES(run=1):
    data = {}
    for compound in compound_types:
        data.update(PATH_DISTANCES(compound, run=run))
    return data


def DATA(run=1):
    with open(DATA_PATH(run=run), 'r') as fp:
        return json.load(fp)


def CLEAN_DATA(run=1):
    with open(CLEAN_DATA_PATH(run=run), 'r') as fp:
        return json.load(fp)


def SIMILARITY():
    with open(SIMILARITY_PATH, 'r') as fp:
        return json.load(fp)


def ALL_GENERATED():
    data = {}
    for filename in ['./run1/head/generated.json', './run1/modifier/generated.json']:
        with open(filename, 'r') as fp:
            merge(json.load(fp), data)
    return data


def GENERATED():
    with open(GENERATED_PATH, 'r') as fp:
        return json.load(fp)


def GENERATED_SIMILARITY():
    with open(GENERATED_SIMILARITY_PATH, 'r') as fp:
        return json.load(fp)


def ATTESTED():
    with open(ATTESTED_PATH, 'r') as fp:
        attested = set([entry.strip() for entry in fp])
    return attested


def FEATURES():
    with open(FEATURES_PATH, 'r') as fp:
        return json.load(fp)


def TERNARY():
    with open(TERNARY_PATH, 'r') as fp:
        return json.load(fp)
