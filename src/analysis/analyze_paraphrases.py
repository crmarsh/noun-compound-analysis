import re
import collections
import nltk
import cleaner
from util import math
import config


def is_preposition_based(paraphrase):
    r = r'.*? (?:(?:is a)|(?:is an)|(?:are)) \w+ (.*)'
    leading_term = re.match(r, paraphrase).group(1).split(' ')[0]
    return leading_term in cleaner.prepositions


def get_paraphrase_body(paraphrase):
    # Extract modifier and head to avoid biasing towards longer compounds
    r = r'(.*?) (?:(?:is a)|(?:is an)|(?:are)) .*'
    lead = re.match(r, paraphrase).group(1)
    lead = re.sub(r'^(?:a|an) ', '', lead)
    (modifier, head) = lead.split(' ')

    # Extract body of paraphrase
    r = r'.*? (?:(?:is a)|(?:is an)|(?:are)) \w+ (.*)'
    body = re.match(r, paraphrase).group(1)
    body = body.replace(modifier, '').replace(head, '').replace('(s)', '').replace('(es)', '')
    body = re.sub(r'\s+', ' ', body).strip()
    return body


def paraphrase_length(paraphrase):
    return len(get_paraphrase_body(paraphrase))


def num_words_in_paraphrase(paraphrase):
    return len(nltk.word_tokenize(get_paraphrase_body(paraphrase)))


def num_distinct_words_in_paraphrase(paraphrase):
    return len(set(nltk.word_tokenize(get_paraphrase_body(paraphrase))))


def get_eccentric(data):
    """Returns the paraphases for which the two other judgments were 'Major difficulty'."""
    paraphrases = []
    for compound in data:
        if data[compound]['score'][config.MAJOR] == 2:
            for i, judgment in enumerate(data[compound]['judgments']):
                paraphrase = judgment['paraphrase']
                rating = judgment['rating']
                if rating != config.MAJOR:
                    paraphrases.append(cleaner.clean_paraphrase(compound, paraphrase))
    return paraphrases

if __name__ == '__main__':
    paraphrases_by_type = collections.defaultdict(list)

    data = config.DATA(run=1)
    # data.update(config.DATA(run=3))
    for compound in data:
        for i, judgment in enumerate(data[compound]['judgments']):
            paraphrase = judgment['paraphrase']
            rating = judgment['rating']
            if paraphrase:
                paraphrase = cleaner.clean_paraphrase(compound, paraphrase)
                paraphrases_by_type[rating].append(paraphrase)

    # Add eccentric paraphrases
    paraphrases_by_type['Eccentric'] = get_eccentric(data)
    print paraphrases_by_type['Eccentric']

    for compound_type in paraphrases_by_type:
        n = len(paraphrases_by_type[compound_type])
        print compound_type + ':', n

        prep_based = filter(is_preposition_based, paraphrases_by_type[compound_type])
        m = len(prep_based)
        print 'Prepositions:', m

        print 'As fraction:', float(m) / n

        lengths = map(paraphrase_length, paraphrases_by_type[compound_type])
        print 'Average length:', math.average(lengths)

        num_words = map(num_words_in_paraphrase, paraphrases_by_type[compound_type])
        print 'Average # words:', math.average(num_words)

        num_distinct_words = map(
            num_distinct_words_in_paraphrase, paraphrases_by_type[compound_type])
        print 'Average # distinct words:', math.average(num_distinct_words)
