import json
from collections import defaultdict
from nltk import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords

MAJOR = 'Major'
EASY = 'Easy'
NONE = 'None'
MINOR = 'Minor'
CONFUSED = 'Confused'


def get_peers(synset):
    """Get the peers for a given synset, i.e., other synsets that share a hypernym."""
    peers = []
    for hypernym in synset.hypernyms():
        for child in hypernym.hyponyms():
            if child != synset:
                peers.append(child)
    return peers


def is_stopword(x):
    return x not in stopwords.words('english')


def similarity_by_path(sense1, sense2, option='path'):
    """Returns maximum path similarity between two senses."""
    if option.lower() in ['path', 'path_similarity']:  # Path similaritys
        return max(wn.path_similarity(sense1, sense2),
                   wn.path_similarity(sense1, sense2))
    elif option.lower() in ['wup', 'wupa', 'wu-palmer', 'wu-palmer']:  # Wu-Palmer
        return wn.wup_similarity(sense1, sense2)
    elif option.lower() in ['lch', 'leacock-chordorow']:  # Leacock-Chodorow
        if sense1.pos != sense2.pos:  # lch can't do diff POS
            return 0
        return wn.lch_similarity(sense1, sense2)


def max_similarity(context_sentence, ambiguous_word, option='path', pos=None, best=True):
    """
    Perform WSD by maximizing the sum of maximum similarity between possible
    synsets of all words in the context sentence and the possible synsets of the
    ambiguous words (see http://goo.gl/XMq2BI):
    {argmax}_{synset(a)}(\sum_{i}^{n}{{max}_{synset(i)}(sim(i,a))}
    """
    result = {}
    for i in wn.synsets(ambiguous_word):
        try:
            if pos and pos != str(i.pos()):
                continue
        except:
            if pos and pos != str(i.pos):
                continue
        words = filter(is_stopword,  word_tokenize(context_sentence))
        result[i] = sum(max([similarity_by_path(i, k, option=option) for k in wn.synsets(j)] + [0])
                        for j in words)

    if option in ['res', 'resnik']:  # lower score = more similar
        result = sorted([(v, k) for k, v in result.items()])
    else:  # higher score = more similar
        result = sorted([(v, k) for k, v in result.items()], reverse=True)
    print result
    if best:
        return result[0][1]
    return result

if __name__ == '__main__':
    with open('main.json', 'r') as fp:
        data = json.load(fp)

    # Sum up scores for various categories
    sums = defaultdict(int)
    for compound in data:
        scores = data[compound]['score']
        if scores['Major'] >= 2:
            sums[MAJOR] += 1
        elif scores['None'] == 3:
            sums[EASY] += 1
        elif scores['None'] == 2:
            sums[NONE] += 1
        elif scores['Minor'] >= 2:
            sums[MINOR] += 1
        else:
            sums[CONFUSED] += 1
    print sums

    # Generate synsets for a given compound
    for compound in data:
        # If the compound is too difficult to analyze, skip
        if data[compound]['score']['Major'] >= 2:
            continue

        modifier = compound.split(' ')[0]
        head = compound.split(' ')[1]
        print 'Compound: ' + modifier + ' ' + head

        for judgment in data[compound]['judgments']:
            paraphrase = judgment['paraphrase']

            # If the judgment were "Major difficulty", no paraphrase would've been provided
            if not paraphrase:
                continue

            print 'Paraphrase: ' + paraphrase

            best_synset = max_similarity(paraphrase, modifier, pos=wn.NOUN)
            print best_synset.definition()
            print best_synset.examples()
            print get_peers(best_synset)
