import json
import re
from collections import defaultdict
from sys import maxint
from nltk.corpus import wordnet as wn
import config

with open(config.DATA_PATH, 'r') as fp:
    data = json.load(fp)

frequencies = {}
distances = {}
labels = {}
counts = defaultdict(int)

compounds = data.keys()


def compute_label(score):
    if score['Major'] >= 2:
        return 'Major'
    elif score['Minor'] >= 2:
        return 'Minor'
    elif score['None'] >= 2:
        return 'None'
    return 'Interpretable'

_distance_cache = {}


def compute_distance(compound):
    def get_distances(compound):
        # lookup in cache
        if compound in _distance_cache:
            return _distance_cache[compound]

        try:
            (modifier, head) = compound.split(' ')
            s1 = wn.synsets(modifier, wn.NOUN)[0]
            s2 = wn.synsets(head, wn.NOUN)[0]
            parent = s1.lowest_common_hypernyms(s2)[0]
            distances = (s1.shortest_path_distance(parent), s2.shortest_path_distance(parent))
        except:
            distances = (maxint, maxint)

        # add to cache before returning
        _distance_cache[compound] = distances
        return distances

    (dist1, dist2) = get_distances(compound)
    return max(dist1, dist2)

for compound in compounds:
    filename = 'ngrams/' + compound.replace(' ', '') + '-eng_2012-1800-2000-3-caseSensitive.csv'
    with open(filename, 'r') as fp:
        # Compute frequency
        frequency = re.search(r'2000\,(.*)', fp.read())
        if frequency:
            frequency = float(frequency.group(1))
        else:
            frequency = 0.0
        frequencies[compound] = frequency

        # Compute distances
        distances[compound] = compute_distance(compound)

        # Compute label
        labels[compound] = compute_label(data[compound]['score'])

        # Compute counts (for random-guessing baseline)
        counts[labels[compound]] += 1

print "Baseline:", 1.0 * max(counts.values()) / sum(counts.values())

split = 200
x_train = []
y_train = []
features = [frequencies, distances]
for compound in compounds[:split]:
    feature_vector = [f[compound] for f in features]
    x_train.append(feature_vector)
    y_train.append(labels[compound])

x_test = []
y_test = []
for compound in compounds[split:]:
    feature_vector = [f[compound] for f in features]
    x_test.append(feature_vector)
    y_test.append(labels[compound])

# Learn
from sklearn import svm
clf = svm.SVC()
clf.fit(x_train, y_train)

errors = 0
correct = 0
for i in range(len(x_test)):
    if clf.predict(x_test[i]) == y_test[i]:
        correct += 1
    else:
        errors += 1

print "Model accuracy:", 1.0 * correct / (errors + correct)
