from collections import defaultdict
from nltk.corpus import wordnet as wn


def get_peers(synset):
    """Get the peers for a given synset, i.e., other synsets that share a hypernym."""
    peers = []
    for hypernym in synset.hypernyms():
        for child in hypernym.hyponyms():
            if child != synset:
                peers.append(child)
    return map(lambda x: x.name(), peers)


if __name__ == '__main__':
    # Hand-labeled synset assignments based on paraphrases
    synset_assignments = {
        u'organ area': [u'organ.n.01', u'organ.n.05', u'organ.n.05']
    }

    peers = defaultdict(dict)

    for compound in synset_assignments:
        valid_synsets_names = set(synset_assignments[compound])
        peers[compound] = {}

        for synset_name in valid_synsets_names:
            peers[compound][synset_name] = get_peers(synset_name)

    print peers
