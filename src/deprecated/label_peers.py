from ngrams.getngrams import runQuery

if __name__ == '__main__':
    peers = {
        u'organ area': {
            u'organ.n.05': [u'accordion.n.01', u'celesta.n.01', u'clavichord.n.01', u'clavier.n.02', u'piano.n.01', u'synthesizer.n.02', u'brass.n.02', u'free-reed_instrument.n.01', u'kazoo.n.01', u'ocarina.n.01', u'organ_pipe.n.01', u'pipe.n.04', u'post_horn.n.01', u'whistle.n.03', u'woodwind.n.01'], 'organ.n.01': [u'abdomen.n.01', u'adnexa.n.01', u'ambulacrum.n.01', u'ampulla.n.01', u'apparatus.n.02', u'area.n.03', u'back.n.01', u'buttock.n.01', u'buttocks.n.01', u'cannon.n.05', u'dilator.n.01', u'dock.n.06', u'dorsum.n.02', u'energid.n.01', u'external_body_part.n.01', u'feature.n.02', u'flank.n.04', u'fornix.n.01', u'gaskin.n.01', u'groove.n.03', u'haunch.n.01', u'hindquarters.n.02', u'hip.n.01', u'horseback.n.01', u'joint.n.01', u'lobe.n.01', u'loin.n.02', u'loins.n.02', u'mentum.n.03', u'partition.n.03', u'process.n.05', u'rectum.n.01', u'rudiment.n.02', u'saddle.n.06', u'shank.n.02', u'shin.n.01', u'shoulder.n.01', u'small.n.01', u'structure.n.04', u'stump.n.02', u'system.n.06', u'thorax.n.01', u'thorax.n.02', u'thorax.n.03', u'tissue.n.01', u'toe.n.03', u'torso.n.01', u'underpart.n.01', u'venter.n.04', u'withers.n.01']
        }
    }

    for compound in peers:
        (modifier, head) = compound.split(' ')

        for head_synset in peers[compound]:
            for modifier_synset in peers[compound][head_synset]:
                new_modifier = modifier_synset.split('.')[0]
                new_compound = new_modifier + ' ' + head
                runQuery(new_compound)
