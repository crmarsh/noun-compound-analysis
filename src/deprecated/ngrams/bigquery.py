import argparse
import os
import subprocess
import json

directory = 'logs'


def get_filename(modifier, head):
    return directory + '/' + modifier + '_' + head + '.txt'


def get_score(modifier, head, overwrite=False):

    filename = get_filename(modifier, head)
    if os.path.isfile(filename):
        if overwrite:
            os.remove(filename)
        else:
            with open(filename, 'r') as f:
                return float(f.read())

    # format and run query
    query = 'bq query --max_rows 5000 --format json  "SELECT first, second, third, cell.value, cell.volume_fraction as count FROM publicdata:samples.trigrams WHERE ((first = \'%s\' AND second = \'%s\') OR (second = \'%s\' AND third = \'%s\')) AND cell.value = \'2008\' GROUP BY first, second, third, count, cell.value"' % (
        modifier, head, modifier, head)
    output = subprocess.check_output(query, shell=True)
    print output

    # discard junk first line and parse json
    try:
        result = json.loads('\n'.join(output.split('\n')[1:]))
        counts = map(lambda x: float(x['count']), result)
        total = reduce(lambda x, y: x + y, counts)
    except ValueError:
        total = 0

    with open(filename, 'w') as f:
        f.write(str(total))

    return total

if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser(description='Get occurrence counts via BigQuery.')
    parser.add_argument('modifier', help='Modifier for the given compound.')
    parser.add_argument('head', help='Head for the given compound.')
    parser.add_argument('--overwrite', action='store_true')
    args = parser.parse_args()

    print get_score(args.modifier, args.head, overwrite=args.overwrite)
