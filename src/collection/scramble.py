import argparse
import json
import logging
import random

import compound

if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser(description='Scramble a set of noun compounds.')
    parser.add_argument('filename', help='Filename containing existing noun compounds.')
    parser.add_argument('N', type=int, help='Number of new compounds to output.')
    args = parser.parse_args()

    # logging config
    logging.basicConfig(filename='scramble.log', level=logging.DEBUG)
    logging.debug("Running scramble.py with: N=%d, data=%s" % (args.N, args.filename))

    # collect all modifiers and heads
    modifiers = []
    heads = []
    all_compounds = set([])
    with open(args.filename, 'r') as f:
        for line in f:
            line = line.strip()
            all_compounds.add(line)
            print line
            (modifier, head) = line.split(' ')
            modifiers.append(modifier)
            heads.append(head)

    def random_compound():
        return compound.Compound(random.choice(heads), random.choice(modifiers))

    # avoid infinite loop by truncating to max_pairs
    max_pairs = len(heads) * len(modifiers)
    N = min(args.N, max_pairs)

    print len(all_compounds)
    print len(set(modifiers)), len(set(heads))

    # generate N distinct random (modifier, head) pairs
    compounds = set([])
    print "["
    for i in range(N):
        if i > 0:
            print ", "

        c = random_compound()
        while c in compounds or c.frame is None:
            c = random_compound()
        logging.debug("Analyzing: " + str(c))
        compounds.add(c)
        logging.debug("Score: " + str(c.score))
        print json.dumps(c.to_serialized())
    print "]"

    # print json.dumps([c.to_serialized() for c in compounds])
