from nltk import pos_tag, word_tokenize, RegexpParser
from nltk.tag.stanford import POSTagger
import inflect
import browse
from util import Singleton


class Tagger(object):
    """Singleton variant of the Stanford Tagger."""
    __metaclass__ = Singleton

    _nltk_data = '../data/nltk'
    _stanford_tagger = POSTagger(
        _nltk_data + '/english-left3words-distsim.tagger',
        _nltk_data + '/stanford-postagger.jar',
        encoding='utf8'
    )

    @classmethod
    def is_valid(cls):
        return not (cls._stanford_tagger is None)

    @classmethod
    def tag(cls, text):
        return cls._stanford_tagger.tag(text)


def get_score(compound):
    return browse.results_for_compound(compound)


def get_snippets(compound, n=1000):
    return browse.snippets_for_compound(compound, n=n)


def get_valid_snippets(compound, n=1000):
    snippets = browse.snippets_for_compound(compound, n=n)
    return filter(lambda s: is_valid_usage(compound, s), snippets)


def generate_inflections(word):
    """
    Generate all inflected forms for a given word, using both classical and standard inflections.
    """
    p = inflect.engine()
    variants = set([word])

    # Inflection methods return `False` if there's no valid result
    def add(f):
        v = f(word)
        if v:
            variants.add(v)

    # Check for classical variants
    p.classical(all=True)
    add(p.plural_noun)
    add(p.singular_noun)

    # Check for standard variants
    p.classical(all=False)
    add(p.plural_noun)
    add(p.singular_noun)

    return variants


def _tag(sentence):
    """Tag a sentence, preferring the Stanford tagger, if available."""
    words = word_tokenize(sentence)
    if Tagger.is_valid():
        return Tagger.tag(words)
    else:
        return pos_tag(words)


def _get_chunks(sentence):
    """
    Chunks using a RegexpParser that looks for chunks following the general format of adjectives
    followed by a single or muliple singular or plural nouns.
    """
    pattern = 'NP: {<DT>?<JJ.*>*(<NN>|<NNS>)+}'
    cp = RegexpParser(pattern)
    return cp.parse(_tag(sentence))


def is_valid_usage(compound, snippet):
    """
    Checks if `snippet` contains a valid usage of `compound`; that is, whether `compound` occurs in
    snippet _as a noun compound_, where compound status is determined through shallow parsing.
    """
    # Give snippet some arbitrary context
    snippet = "Next, look at the following: " + snippet

    # Find representative chunk
    for chunk in _get_chunks(snippet).subtrees():
        if chunk.label() == 'NP':
            words = set([w.lower() for (w, _) in chunk.leaves()])
            if compound.heads.intersection(words) and compound.modifiers.intersection(words):
                return True

    return False
