import analyzer
from framenet import FrameNet


class Compound(object):

    # Score computation requires ~(SCORE_LIMIT / 25) minutes
    SCORE_LIMIT = 100

    def __init__(self, head, modifier):
        self.head = head
        self.modifier = modifier

        # gather inflected forms
        self.heads = analyzer.generate_inflections(self.head)
        self.modifiers = analyzer.generate_inflections(self.modifier)

        # rest of the properties are computed lazily
        self._variants = None
        self._score = None
        self._frames = None
        self._frame = None
        self._all_categories = None
        self._categories = None
        self._snippets = None
        self._valid_snippets = None

    def __str__(self):
        return self.modifier + ' ' + self.head

    def __repr__(self):
        return 'Compound(head="' + self.head + '", modifier="' + self.modifier + '")'

    def __eq__(self, other):
        return self.head == other.head and self.modifier == other.modifier

    def to_query_string(self):
        return '"' + str(self) + '"'

    def to_serialized(self):
        # To save space, only store index of valid snippets
        vs = set(self.valid_snippets)
        return {
            'head': self.head,
            'modifier': self.modifier,
            'score': self.score,
            'snippets': self.snippets,
            'valid_snippets': [i for (i, s) in enumerate(self.snippets) if s in vs],
            'max': self.SCORE_LIMIT,
            'frame': self.frame,
            'elements': list(self.possible_categories)
        }

    @property
    def variants(self):
        if self._variants is None:
            self._variants = set([])
            for head in self.heads:
                for modifier in self.modifiers:
                    self._variants.add(modifier + ' ' + head)

        return self._variants

    @property
    def score(self):
        """Get the relevant score for a given noun compound, based on search engine results.
        >>> Compound('oil', 'olive').score
        11800000
        """
        if self._score is None:
            self._score = len(self.valid_snippets)

        return self._score

    @property
    def snippets(self):
        if self._snippets is None:
            self._snippets = analyzer.get_snippets(self, n=self.SCORE_LIMIT)

        return self._snippets

    @property
    def valid_snippets(self):
        if self._valid_snippets is None:
            self._valid_snippets = filter(lambda s: analyzer.is_valid_usage(self, s), self.snippets)

        return self._valid_snippets

    @property
    def frames(self):
        """Get the relevant frames for a given noun compound, based on its head.
        >>> Compound('oil', 'olive').frames
        set(['Food', 'Substance'])
        """
        if self._frames is None:
            self._frames = set([])
            framenet = FrameNet()
            for head in self.heads:
                self._frames.update(framenet.get_frames_for_noun(head))

        return self._frames

    @property
    def all_possible_categories(self):
        """Get the relevant frame elements for a given noun compound, based on its head.
        >>> Compound('boat', 'speed').possible_categories
        set(['Descriptor', 'Use', 'Possessor', 'Itinerary'])
        """
        if self._categories is None:
            self._categories = set([])
            framenet = FrameNet()
            for frame in self.frames:
                self._categories.update(framenet.get_elements_for_frame(frame))

        return self._categories

    @property
    def frame(self):
        """Return the frame for this noun compound, chosen at random from the available frames."""
        if self._frame is None:
            try:
                self._frame = self.frames.pop()
                self.frames.add(self._frame)
            except:
                pass

        return self._frame

    @property
    def possible_categories(self):
        """
        Get the relevant frame elements for a given noun compound, based on its head. Only uses
        the frame identified by self.frame.
        """
        if self._categories is None:
            framenet = FrameNet()
            self._categories = framenet.get_elements_for_frame(self.frame)

        return self._categories


if __name__ == '__main__':
    import doctest
    doctest.testmod()
