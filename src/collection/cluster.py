from sklearn.cluster import AffinityPropagation
from nltk.corpus import wordnet as wn
import numpy as np


def cluster(words):
    """Cluster words using the AffinityPropagation algorithm."""
    synsets = [wn.synset(w + '.n.01') for w in words]
    X = np.matrix([[wn.path_similarity(i, j) for j in synsets] for i in synsets])
    af = AffinityPropagation().fit(X)
    cluster_centers_indices = af.cluster_centers_indices_
    labels = af.labels_
    n_clusters_ = len(cluster_centers_indices)
    return n_clusters_, labels
