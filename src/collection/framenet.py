import xml.etree.ElementTree as ET
from collections import defaultdict
from util import Singleton

path_to_data = '../data/fndata-1.5'


class FrameNet(object):
    __metaclass__ = Singleton

    LU_INDEX = path_to_data + '/luIndex.xml'
    # only extract frame elements that are sufficiently important
    VALID_FE_TYPE = set(['Core', 'Peripheral'])
    # ignore frame elements that are too general to be of interest
    INVALID_FE = set(['Relative_location', 'Type', 'Owner'])

    def __init__(self):
        self._lu_index = None
        self._frames_for_name = None
        self._elements_for_frame = None

    @property
    def lu_index(self):
        if self._lu_index is None:
            tree = ET.parse(FrameNet.LU_INDEX)
            self._lu_index = tree.getroot()

        return self._lu_index

    @property
    def frames_for_name(self):
        if self._frames_for_name is None:
            self._frames_for_name = defaultdict(set)
            for lexical_unit in self.lu_index.findall('{http://framenet.icsi.berkeley.edu}lu'):
                name = lexical_unit.get('name')
                frame = lexical_unit.get('frameName')
                self._frames_for_name[name].add(frame)

        return self._frames_for_name

    @classmethod
    def _filename_for_frame(cls, frame):
        return path_to_data + '/frame/' + frame + '.xml'

    def get_frames_for_name(self, name):
        """Return the frames relevant to a given name.
        >>> FrameNet().get_frames_for_name('oil.n')
        set(['Food', 'Substance'])
        """
        return self.frames_for_name[name]

    def get_frames_for_noun(self, noun):
        """Return the frames relevant to a given noun.
        >>> FrameNet().get_frames_for_noun('oil')
        set(['Food', 'Substance'])
        """
        return self.frames_for_name[noun + '.n']

    def get_elements_for_frame(self, frame, filtered=True):
        """Return the frame elements for a given frame.
        >>> FrameNet().get_elements_for_frame('Food')
        set(['Descriptor', 'Constituent_parts'])
        >>> FrameNet().get_elements_for_frame('Food', filtered=False)
        set(['Food', 'Descriptor', 'Type', 'Constituent_parts'])
        """
        elements = set([])
        with open(FrameNet._filename_for_frame(frame), 'r') as f:
            frame_index = ET.parse(f).getroot()
            for element in frame_index.findall('{http://framenet.icsi.berkeley.edu}FE'):
                if element.get('coreType') in FrameNet.VALID_FE_TYPE:
                    name = element.get('name')
                    if not filtered or (name != frame and not name in FrameNet.INVALID_FE):
                        elements.add(name)

        return elements

    def get_descriptor_for_frame_element(self, frame, element):
        with open(FrameNet._filename_for_frame(frame), 'r') as f:
            frame_index = ET.parse(f).getroot()
            for e in frame_index.findall('{http://framenet.icsi.berkeley.edu}FE'):
                if e.get('name') == element:
                    for child in e.findall('{http://framenet.icsi.berkeley.edu}definition'):
                        return child.text

if __name__ == '__main__':
    import doctest
    doctest.testmod()
