import unittest
import analyzer
from compound import Compound
from cluster import cluster
from bigquery import get_score


class TestNGrams(unittest.TestCase):

    def testBigQuery(self):
        modifier = 'jungle'
        head = 'gym'
        self.assertEqual(get_score(modifier, head), 0.00356333009394)


class TestClustering(unittest.TestCase):

    def testBasic(self):
        words = ['dog', 'cat', 'hit', 'slap']
        (num_clusters, labels) = cluster(words)
        self.assertEqual(num_clusters, 2)
        self.assertEqual(list(labels), [0, 0, 1, 1])


class TestInflection(unittest.TestCase):

    def testBasic(self):
        """Does it generate basic inflections?"""
        word = 'fox'
        variants = analyzer.generate_inflections(word)
        self.assertEqual(variants, set(['fox', 'foxes']))

    def testClassical(self):
        """Does it include classical forms?"""
        word = 'focus'
        variants = analyzer.generate_inflections(word)
        self.assertEqual(variants, set(['foci', 'focuses', 'focus', 'focu']))


class TestValidator(unittest.TestCase):

    def testCorrect(self):
        """Does the validator allow for correct usages?"""
        compound = Compound('oil', 'olive')
        snippet = "The health benefits of olive oil are extensive with new positive attributes " + \
            "discovered all the time. One prominent cardiologist recommends at least two"
        self.assertTrue(analyzer.is_valid_usage(compound, snippet))

    def testPlural(self):
        """Does the validator capture inflected forms?"""
        compound = Compound('oil', 'olive')
        snippet = "Yearly index of the best olive oils in the world and the results of the NY " + \
            "International Olive Oil Competition."
        self.assertTrue(analyzer.is_valid_usage(compound, snippet))

    def testProperNouns(self):
        """Does the validator guard against proper nouns?"""
        compound = Compound('cigarette', 'bacon')
        snippet = "Shop 1000s of Roger Bacon Cigarette Pig Jolly T Shirt Designs Online! Find " + \
            "All Over Print, Classic, Fashion, Fitted, Maternity, Organic, and V Neck Tees."
        self.assertFalse(analyzer.is_valid_usage(compound, snippet))

    def testPunctuation(self):
        """Does the validator guard against punctuation-separated compounds?"""
        compound = Compound('cigarette', 'bacon')
        snippet = "Bacon, cigarette prices increase. To many of the economic minds on Wall " + \
            "Street or in the White House, there is no price inflation"
        self.assertFalse(analyzer.is_valid_usage(compound, snippet))


class TestCompound(unittest.TestCase):

    def testEquality(self):
        """Are equivalent compounds considered equal?"""
        self.assertEqual(Compound('oil', 'olive'), Compound('oil', 'olive'))

    def testVariants(self):
        """Does it generate variants for compounds?"""
        compound = Compound('oil', 'olive')
        headInflections = analyzer.generate_inflections(compound.head)
        modifierInflections = analyzer.generate_inflections(compound.modifier)
        self.assertEqual(len(compound.variants), len(headInflections) * len(modifierInflections))

    @unittest.skip("Skipping slow score computation.")
    def testScore(self):
        compound = Compound('oil', 'olive')
        self.assertTrue(compound.score > 0)

if __name__ == '__main__':
    unittest.main()
