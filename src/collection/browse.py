from time import sleep
from random import randint
import urllib
import urllib2
import json
import re
from splinter import Browser


MAX_WAIT_TIME = 10
MIN_WAIT_TIME = 2


def _wait():
    sleep(randint(MIN_WAIT_TIME, MAX_WAIT_TIME))


def _stagger(func):
    def inner(*args, **kwargs):
        result = func(*args, **kwargs)
        _wait
        return result
    return inner


@_stagger
def results_for_compound(compound):
    url = 'https://www.google.com/#q=%22' + compound.modifier + '+' + compound.head + '%22'

    with Browser("chrome") as browser:
        # Visit URL
        browser.visit(url)

        # Extract and parse number of results
        results_text = browser.find_by_id('resultStats').first.value
        try:
            results_value = re.match("About (.*) results", results_text).group(1)
            return int(results_value.replace(',', ''))
        except:
            return 0


@_stagger
def _snippets_for_compound(compound, n=10):
    url = 'https://www.google.com/#q=%22' + compound.modifier + '+' + compound.head + '%22'

    with Browser("chrome") as browser:
        snippets = []

        # Visit URL
        browser.visit(url)

        while len(snippets) < n:
            # Extract snippets from the results page
            snippets += [s.value for s in browser.find_by_css('.st')]

            # Click to next
            browser.find_by_id('pnnext').first.click()
            _wait()
        return snippets


def snippets_for_compound(compound, n=50):
    """
    Returns the n most relevant search results for a given compound. If fewer than n results are
    available, then it returns as many as it can.
    """

    def format_url(query, skip):
        return 'https://api.datamarket.azure.com/Data.ashx/Bing/Search/Web?Query=%27' + \
            query + '%27&$top=50&$skip=' + str(skip) + '&$format=json'

    # Construct query
    query = urllib.quote(compound.to_query_string())

    # Create credential for authentication
    key = 'fYPHp3gT5xRhbso65WyApVwGEgGnxOcfjOkG1g21Nr8'
    user_agent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; FDM; ' + \
        '.NET CLR 2.0.50727; InfoPath.2; .NET CLR 1.1.4322)'
    credentials = (':%s' % key).encode('base64')[:-1]
    auth = 'Basic %s' % credentials

    results = []
    while len(results) < n:
        request = urllib2.Request(format_url(query, len(results)))
        request.add_header('Authorization', auth)
        request.add_header('User-Agent', user_agent)
        request_opener = urllib2.build_opener()
        response = request_opener.open(request)

        # Parse response
        response_data = response.read()
        json_result = json.loads(response_data)['d']['results']
        if not json_result:
            break
        results += json_result

    descriptions = [r[u'Description'] for r in results]
    return descriptions

if __name__ == '__main__':
    from compound import Compound
    c = Compound('cure', 'glass')
    print c.to_serialized()
