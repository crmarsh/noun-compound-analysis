from sys import exit
import argparse
from random import shuffle
import json
import inflect

p = inflect.engine()


def generate_phrase(modifier, head):
    head_is_plural = bool(p.singular_noun(head))
    if head_is_plural:
        prefix_mod = modifier
        prefix_head = head
        prep = 'are'
    else:
        prefix_mod = p.a(modifier)
        prefix_head = p.a(head)
        prep = 'is'

    modifier_is_plural = bool(p.singular_noun(modifier))
    suffix_mod = modifier
    if not modifier_is_plural:
        plural_mod = p.plural_noun(modifier)
        if len(plural_mod) > len(modifier):
            prefix = plural_mod[:len(modifier)]
            suffix = plural_mod[len(modifier):]
            if prefix == modifier and (suffix == 's' or suffix == 'es'):
                suffix_mod = modifier + '(' + suffix + ')'

    return prefix_mod + ' ' + head + ' ' + prep + ' ' + prefix_head + ' that [...] ' + suffix_mod


def get_filename(i, j):
    return 'mturk/' + str(i) + '_' + str(j) + '.csv'


if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser(
        description='Convert results to Amazon Mechanical Turk format.')
    parser.add_argument(
        'filenames', nargs='+', help='Filename containing annotated noun compounds.')
    parser.add_argument('num_compounds', type=int, help='Number of compounds per batch.')
    parser.add_argument('num_judges', type=int, help='Number of judges per compound.')
    args = parser.parse_args()

    # gather list of all compounds
    all_compounds = set([])
    for filename in args.filenames:
        with open(filename, 'r') as f:
            for line in f:
                (mod, head) = line.strip().split(' ')
                all_compounds.add((mod, head))
    all_compounds = list(all_compounds)

    num_batches = len(all_compounds) / args.num_compounds
    if len(all_compounds) % args.num_compounds != 0:
        num_batches += 1

    for i in range(args.num_judges):
        shuffle(all_compounds)
        for j in range(num_batches):
            with open(get_filename(i, j), 'w+') as f:
                f.write('word1, word2, phrase\n')
                end = min((j + 1) * args.num_compounds, len(all_compounds))
                for c in all_compounds[j * args.num_compounds:end]:
                    modifier = c[0]
                    head = c[1]
                    phrase = generate_phrase(modifier, head)
                    f.write('%s, %s, %s\n' % (modifier, head, phrase))
